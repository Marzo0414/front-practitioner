import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement{
    static get properties () {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},            
        };
    }


    constructor () {        super ();
        this.people = [
            {
                name: "Persona 1",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona1.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada erat eu varius porttitor. Maecenas id posuere mauris. Pellentesque varius, est eget rhoncus pharetra, urna sem interdum risus, pharetra pretium libero lorem nec turpis."
            },
            {
                name: "Persona 2",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

            },
            {   
                name: "Persona 3",
                yearsInCompany: 3,
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non gravida arcu, non posuere justo. Mauris vulputate tellus ut erat sodales, viverra mattis augue ornare. Cras nec laoreet ex. Donec vel est ac metus vestibulum semper. Proin pharetra placerat eros, at laoreet nisl viverra sed."

            },
            {
                name: "Persona 4",
                yearsInCompany: 3,
                photo: {
                    src: "./img/persona4.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tristique bibendum tincidunt. Nunc tempor lorem sit amet lacus sodales, ut cursus nisi condimentum. Sed ultrices vestibulum risus a rhoncus. Nam auctor tortor magna, sed congue nunc pulvinar a. Duis id elit diam. Curabitur convallis maximus purus quis sodales. Phasellus nec enim diam."

            },
            {
                name: "Persona 5",
                yearsInCompany: 3,
                photo: {
                    src: "./img/persona5.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id augue et massa congue auctor. Suspendisse velit orci, fermentum placerat justo vitae, semper placerat arcu. Sed libero leo, blandit aliquet purus et, accumsan ullamcorper justo. In in dapibus eros. Nullam vitae erat eu est vehicula laoreet. Vivamus commodo diam nisl, ut ullamcorper tortor viverra ac."
            },
            {
                name: "Persona 6",
                yearsInCompany: 3,
                photo: {
                    src: "./img/persona6.jpg",
                    alt: "No hay foto"
                },
                profile: "Lorem ipsum dolor sit amet. Donec id augue et massa congue auctor. Suspendisse velit orci, fermentum placerat justo vitae, semper placerat arcu. Sed libero leo, blandit aliquet purus et, accumsan ullamcorper justo. In in dapibus eros. Nullam vitae erat eu est vehicula laoreet. Vivamus commodo diam nisl, ut ullamcorper tortor viverra ac."
            }            
        ];
    }

        updated(changedProperties) {
            console.log ("updated en persona-main-dm");

            if (changedProperties.has("people")) {
                console.log ("Ha cambiado el valor de la propiedad people en persona-main-dm");

                this.dispatchEvent(new CustomEvent ("people-data-updated",
                {
                    detail: {
                        people: this.people
                    }
                }
                ))
            }
        }

}

customElements.define ("persona-main-dm", PersonaMainDM);