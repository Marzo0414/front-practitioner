import { LitElement, html } from "lit-element";
import "../emisor-evento/emisor-evento";
import "../receptor-evento/receptor-evento";

class GestorEvento extends LitElement{
    static get properties () {
        return {
        };
    }

    constructor () {
        super ();
    }

    render () {
        return html `
        <h1>Gestor Evento</h1>
        <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
        <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent (e) {
        console.log ("processEvent");
        console.log ("Capturando el evento del emisor");
        console.log (e);
        console.log (e.detail);
        
        this.shadowRoot.getElementById ("receiver").course = e.detail.course;
        this.shadowRoot.getElementById ("receiver").year =   e.detail.year;
    }
}

customElements.define ("gestor-evento", GestorEvento);