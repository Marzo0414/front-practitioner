import { LitElement, html } from "lit-element";

class PersonaSidebar extends LitElement{
    static get properties () {
        return {
            peopleStats: {type: Object},
            yearsRange:  {type: Number}
        };
    }


    constructor () {
        super ();
        this.peopleStats = {};
    }

    render () {
        return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <div class="form-group">
            <label>Años en la empresa</label>
            <input @input="${this.updateYearsRange}" .value="${this.yearsRange}" type="text" class="form-control" placeholder="Años en la empresa" />
        </div>
        <aside>
            <section>
                <div>
                    Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas
                </div>              
                <div class="mt-5">
                    <button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
                </div>
                <div>
                    <input   type="range" min="0" 
                    max="${this.peopleStats.maxYearsInCompany}" 
                    step ="1" 
                    .value="${this.peopleStats.maxYearsInCompany}"
                    @input="${this.updateMaxYearsInCompany}"
                    />                
                </div>
            </section>
        </aside>
        `;
    }

    newPerson (e) {
        console.log ("newPerson en persona-sidebar");
        console.log ("Se va a crear una nueva persona");

        this.dispatchEvent (new CustomEvent ("new-person", {}));
    }

    updateMaxYearsInCompany (e) {
        console.log ("updateMaxYearsInCompany en persona-sidebar");
        console.log ("Actualizando el valor de la propiedad maxYearsInCompany de person con valor " + e.target.value);

        this.dispatchEvent (new CustomEvent("update-max-years-filter", {
            detail: {
                maxYearsInCompany: e.target.value
            }
        }));

    }

}

customElements.define ("persona-sidebar", PersonaSidebar);