import { LitElement, html } from "lit-element";
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement{
    static get properties () {
        return {
                people: {type: Array}
        };
    }


    constructor () {
        super ();
}

    render () {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar 
                    @new-person="${this.newPerson}" 
                    @update-max-years-filter="${this.updateMaxYearInCompanyFilter}" 
                    class="col-2">
                </persona-sidebar>
                <persona-main    @updatedpeople="${this.updatePeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats  @updated-people-stats= "${this.updatePeopleStats}"></persona-stats>
        `;
    }

    updated (changedProperties) {
        console.log("updated en persona-app");
        console.log(changedProperties);

        if (changedProperties.has ("people")) {
            console.log ("Ha cambiado la propiedad people en persona-app");
            this.shadowRoot.querySelector ("persona-stats").people = this.people;
        }
    }


    newPerson (e) {
        console.log ("newPerson en persona-app");
        this.shadowRoot.querySelector ("persona-main").showPersonForm = true;
        }

    updateMaxYearInCompanyFilter (e) {
        console.log ("updateMaxYearInCompanyFilter en persona-app");
        console.log ("El nuevo filtro es " + e.detail.maxYearsInCompany);
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompany;
        }

    updatePeople (e) {
        console.log ("updatePeople en persona-app");        
        this.people = e.detail.people;
        
    }

    updatePeopleStats (e) {
        console.log ("updatePeople en persona-app");
        console.log (e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;
    }
}

customElements.define ("persona-app", PersonaApp);